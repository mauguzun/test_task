<?php
declare(strict_types=1);
use Model\Database;
use Pages\Add;

require_once "Model/Model.php";
require_once "Pages/Add.php";

$page = new Add(new Database());

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $page->isPostBack($_POST);
}

$page->show("Views/view.add.php");
