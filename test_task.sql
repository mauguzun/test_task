/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : test_task

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2018-03-30 22:14:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for Param
-- ----------------------------
DROP TABLE IF EXISTS `Param`;
CREATE TABLE `Param` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Param
-- ----------------------------
INSERT INTO `Param` VALUES ('1', '1', 'width', 'mm');
INSERT INTO `Param` VALUES ('2', '1', 'height', 'mm');
INSERT INTO `Param` VALUES ('3', '1', 'length', 'mm');
INSERT INTO `Param` VALUES ('4', '2', 'size', 'mb');
INSERT INTO `Param` VALUES ('5', '3', 'weight', 'kg');

-- ----------------------------
-- Table structure for Product
-- ----------------------------
DROP TABLE IF EXISTS `Product`;
CREATE TABLE `Product` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` double(10,2) NOT NULL,
  `type` smallint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Product
-- ----------------------------
INSERT INTO `Product` VALUES ('68', 'iddqd', 'Doom', '77.70', '2');
INSERT INTO `Product` VALUES ('70', 'iddqpois', 'denis', '123.00', '1');
INSERT INTO `Product` VALUES ('71', 'ifdd912312', 'Garry Poter', '11.78', '3');
INSERT INTO `Product` VALUES ('72', 'denis11', 'denis penis', '11.11', '3');
INSERT INTO `Product` VALUES ('73', 'is five', 'five element', '11.10', '2');

-- ----------------------------
-- Table structure for Size
-- ----------------------------
DROP TABLE IF EXISTS `Size`;
CREATE TABLE `Size` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `param_id` int(11) NOT NULL,
  `value` double(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`,`product_id`,`param_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Size
-- ----------------------------
INSERT INTO `Size` VALUES ('52', '68', '4', '700.00');
INSERT INTO `Size` VALUES ('56', '70', '1', '123.00');
INSERT INTO `Size` VALUES ('57', '70', '2', '123123.00');
INSERT INTO `Size` VALUES ('58', '70', '3', '123123.00');
INSERT INTO `Size` VALUES ('59', '71', '5', '11.00');
INSERT INTO `Size` VALUES ('60', '72', '5', '77.00');
INSERT INTO `Size` VALUES ('61', '73', '4', '111.00');

-- ----------------------------
-- Table structure for Type
-- ----------------------------
DROP TABLE IF EXISTS `Type`;
CREATE TABLE `Type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `param_id` int(11) NOT NULL,
  `item_description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Type
-- ----------------------------
INSERT INTO `Type` VALUES ('1', 'furniture', '1', 'Dimension');
INSERT INTO `Type` VALUES ('2', 'cd', '2', 'Size ');
INSERT INTO `Type` VALUES ('3', 'book', '3', 'Weight');
SET FOREIGN_KEY_CHECKS=1;
