<?php


$actions = [
    'delete' => 'Delete',
    //
];


?>
<form method="post" action="">

<div class="container">
    <br>

        <div class="row">
            <div class="col-sm-9">
                <h2>Add Product</h2>
            </div>
            <div class="col-sm-3">
                <input type="submit" class="form-control"/>
            </div>
        </div>
    <div class="row">

        <hr/>

    </div>
</div>

<div class="container">

    <div class="row">

        <div class="col-sm-6">
            <?
                // just lazy pig
               $fields = ['sku','name','price'] ;
            foreach ($fields as $field) :?>
                   <label><?= $field ?></label>
                   <input placeholder="<?= $field ?>"
                          required="required"
                          type="<?= $field == 'price' ? 'number' : 'text'?>"
                            <?=  $field == 'price' ? 'min="0.01" step="0.01" ' : null ; ?>

                          name="<?= $field ?>" class="form-control"/>
            <? endforeach;?>
            <label>Select type</label>
            <select name="type" class="form-control" required="required">
                <option disabled selected value> Please Select</option>
                    <? foreach ($query as $type) :?>
                        <option value="<?= $type['id'] ?>" >  <?= $type['type'] ?> </option>
                    <? endforeach; ?>
            </select>


        </div>
        <div class="col-sm-6" id="other_param">

        </div>
    </div>


</div>
</form>
<script>
    $( "select[name='type']" ).on('change', function () {
        let  result =  document.querySelector("#other_param");
        result.innerHTML = "";

        if (this.value){
             $.post( "Views/Ajax.php", {type: this.value } )
             .done((data)=>{
                 result.innerHTML = data;
            })
            .catch ((error)=>{
                 console.log(error);
            })
        }
    });
</script>

