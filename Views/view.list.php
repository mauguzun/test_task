<?php

// edit , view more etc
$actions = ['delete' => 'Delete']; ?>

<form method="post">
    <div class="container">
        <br>
        <div class="row">
            <div class="col-sm-9">
                <h2>Product List</h2>
            </div>
            <div class="col-sm-2">
                <select class="form-control" required="required" name="action">
                    <option disabled selected value> Please Select</option>
                    <? foreach ($actions as $action => $text) : ?>
                        <option value="<?= $action ?>"><?= $text ?></option>
                    <? endforeach; ?>
                </select>
            </div>
            <div class="col-sm-1">
                <input type="submit" id="sendAction" class="form-control"/>
            </div>
        </div>
        <div class="row">
            <hr/>
        </div>
    </div>

    <div class="container text-center">
        <? if (count($query) > 0) :?>
        <div class="row">
            <? $column = 0;
            foreach ($query as $item) : ?>
                <div class="col-sm-3 my">
                    <span> <input type="checkbox" name="<?= $item['pid'] ?>" value="id" ?></span>
                    <strong> SKU : <?= $item['sku'] ?>    </strong>
                    <p> <?= $item['name'] ?>    </p>
                    <p> <?= $item['price'] ?> $ </p>
                    <small>
                        <? foreach ($item['sizes'] as $key => $size) : ?>
                            <?= $key ?> : <?= $size['value'] ?> <b> <?= $size['unit'] ?>  </b>
                        <? endforeach; ?>
                    </small>
                </div>
                <?= (++$column % 4 == 0) ? "</div><div class='row'>" : null ?>
            <? endforeach; ?>
        </div>

        <? endif; ?>
    </div>
</form>

<script>
    $('.my').on('click', function () {
        let checkbox = $(this).find("input[type='checkbox']");
        checkbox.prop("checked", !checkbox.prop("checked"));
    })
</script>

