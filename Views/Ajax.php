<?php

use Model\Database ;

require_once "../Model/Model.php";
$model  = new Database();
/**
 *  result string for ajax
 */
$result = "";

if (isset($_POST) && isset($_POST['type']) && is_numeric($_POST['type'])) {
    $query = $model->select("Param", "Param.id as pmid ,Param.*,Type.*", ["Type"=>"Param.type_id = Type.id and Type.id='".$_POST['type']."'"], "pmid") ;
    if (!empty($query)) {
        foreach ($query as $row) {
            if (empty($result)) {
                $result .= "<h3>{$row['item_description']}</h3>";
            }
            $result .= "<label>{$row['description']}( {$row['unit']} )</label></lbale><input placeholder='{$row['description']}' type='number' required='required' name='{$row['pmid']}' min='0' step='0.01' class='form-control' />";
        }
    }
}
echo $result;
