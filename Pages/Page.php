<?php
declare(strict_types=1);
namespace Pages;

use Model\IModel;

include_once "Model/Model.php";

/**
 * Class Page just two method
 * @package Pages
 */
abstract class Page
{
    /**
     * @var IModel database connection
     */
    protected $_model;

    /**
     * Page constructor.
     * @param IModel $model can use any class whcih imp IModel
     */
    public function __construct(IModel $model)
    {
        $this->_model = $model;
    }

    /**
     * @param array $data
     * @return mixed
     */
    abstract public function isPostBack(array $data);

    /**
     * @param string $page
     * @return mixed
     */
    abstract public function show(string $page);
}
