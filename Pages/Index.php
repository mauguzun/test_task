<?php
declare(strict_types=1);

namespace Pages;

require_once "Page.php";

use Model\IModel;

/**
 * Class Index
 * @package Pages
 */
class Index extends Page
{
    /**
     * Index constructor.
     * @param IModel $model
     */
    public function __construct(IModel $model)
    {
        parent::__construct($model);
    }

    /**
     * @param array $data
     * send post here
     */
    public function isPostBack(array $data)
    {
        switch ($data) {
            case "delete":
                unset($_POST['action']) ;
                if ($this->_model->delete("Product", $_POST)) {
                    $this->_model->delete("Size", array_fill_keys(array_keys($_POST), "product_id"));
                }
                break;
            // other cases edit , more etc
        }
    }

    /**
     * @param string $page
     * page view go here
     */
    public function show(string $page)
    {
        include_once"Views/Shared/view.header.php";

        $dirtyQuery   = $this->_model->select(
            "Product",
            "Product.id as pid ,Product.*,Type.*,Size.*,Param.*",
            [
                "Type" => "Product.type = Type.id",
                "Param" => "Type.id=Param.type_id",
                "Size" => "Size.param_id = Param.id and Size.product_id = Product.id"
            ],
            'Size.param_id,pid',
            'pid'
        );

        $query = [];
        foreach ($dirtyQuery as $item) {
            if (!array_key_exists($item['pid'], $query)) {
                $query[$item['pid']] = $item;
                $query[$item['pid']]['sizes'] = [];
            }
            $query[$item['pid']]['sizes'][$item['description']]=[ 'unit'=>$item['unit'], 'value'=>$item['value'] ] ;
        }
        include_once $page;
        include_once "Views/Shared/view.footer.php";
    }
}
