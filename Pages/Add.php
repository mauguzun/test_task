<?php
declare(strict_types=1);

namespace Pages;

require_once "Page.php";

use Model\IModel;

/**
 * Class Add
 * @package Pages
 */
class Add extends Page
{
    /**
     * Add constructor.
     * @param IModel $model
     */
    public function __construct(IModel $model)
    {
        parent::__construct($model);
    }

    /**
     * @param array $data
     * send post here
     */
    public function isPostBack(array $data)
    {
        $values = $this->_model->validate($data, ['price']);
        if ($values) {
            $product = $size = [];
            foreach ($values as $key => $value) {
                if (in_array($key, ['sku', 'name', 'price', 'type'])) {
                    $product[$key] = $value;
                } else {
                    array_push($size, ['param_id' => $key, 'value' => $value]);
                }
            }
            if ($values) {
                $product_id = $this->_model->insert("Product", $product);
                foreach ($size as & $row) {
                    $row['product_id'] = $product_id;
                    $this->_model->insert("Size", $row);
                }
            }
            header('Location: '."index.php");
        }
    }

    /**
     * @param string $page
     * pages view
     */
    public function show(string $page)
    {
        include_once"Views/Shared/view.header.php";
        $query   = $this->_model->select("Type", "*", null, null, null, 'type');
        include_once $page;
        include_once "Views/Shared/view.footer.php";
    }
}
