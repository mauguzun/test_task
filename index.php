<?php
use Model\Database;
use Pages\Index;

require_once "Model/Model.php";
require_once "Pages/Index.php";

$page = new Index(new Database());

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $page->isPostBack($_POST);
}

$page->show("Views/view.list.php");
