<?php
namespace  Model;

/**
 * Interface IModel
 * @package Model
 */
interface IModel
{
    /**
     * @param string $table
     * @param array $params
     * @return int|null
     */
    public function insert(string $table, array $params = []): ?int;

    /**
     * @param string $table
     * @param string $rows
     * @param array|null $join
     * @param string|null $group
     * @param string|null $order
     * @return array
     */
    public function select(string $table, string $rows = '*', array $join = null, string $group = null, string $order = null): array;

    /**
     * @param array $clearMe
     * @param array|null $int
     * @return array|null
     */
    public function validate(array $clearMe, array $int = null):?array;

    /**
     * @param string $table
     * @param array $where
     * @return bool
     */
    public function delete(string $table, array $where): bool;

    /**
     *  close database here
     */
    public function __destruct();
}
