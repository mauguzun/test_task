<?php


namespace Model;

include_once "IModel.php";

/**
 * Class Database
 * @package Model
 */
class Database implements IModel
{

    private $db_host = "localhost";
    private $db_user = "root";
    private $db_pass = "";
    private $db_name = "test_task";
    /**
     * @var bool|\mysqli
     * connection
     */
    private $db = false;

    /**
     * Database constructor.
     * or die !
     */
    public function __construct()
    {
        $this->db = new \mysqli($this->db_host, $this->db_user, $this->db_pass, $this->db_name);
        if ($this->db->connect_error) {
            die('Connect Error: ' . $this->db->connect_error);
        }
    }

    /***
     * @param string $table
     * @param array $params
     * @return int|null
     */
    public function insert(string $table, array $params = []): ?int
    {
        $query = 'INSERT INTO `' . $table . '` (`' . implode('`, `', array_keys($params)) . '`) VALUES ("' . implode('", "', $params) . '")';
        $this->db->query($query);
        return $this->db->insert_id;
    }

    /***
     * @param string $table
     * @param string $rows
     * @param array|null $join
     * @param string|null $group
     * @param string|null $order
     * @return array or empty ..
     */
    public function select(
        string $table,
        string $rows = '*',
        array $join = null,
        string $group = null,
        string $order = null
    ): array {
        // Create query from the variables passed to the function
        $q = 'SELECT ' . $rows . ' FROM ' . $table;
        if ($join != null) {
            $this->db->query('SET @@sql_mode = "";');
            foreach ($join as $table => $expression) {
                $q .= ' JOIN  ' . $table . ' ON ' . $expression;
            }
        }
        if ($group != null) {
            $q .= ' GROUP BY  ' . $group;
        }
        if ($order != null) {
            $q .= ' ORDER BY ' . $order;
        }
        //echo $q;
        $values = [];
        if ($result = $this->db->query($q)) {
            while ($row = $result->fetch_assoc()) {
                $values[] = $row;
            }
        }
        return $values;
    }

    /**
     * @param array $clearMe
     * @param array|null $int
     * @return array|null
     */
    public function validate(array $clearMe, array $int = null): ?array
    {
        foreach ($clearMe as $k => &$value) {
            $value = trim($value);
            if (empty($value)) {
                return null;
            } elseif ($int && in_array($k, $int)) {
                if (!is_numeric($value)) {
                    return null;
                }
            }
        }
        return $clearMe;
    }

    /**
     * @param string $table
     * @param array $where
     * @return bool
     */
    public function delete(string $table, array $where): bool
    {

        $delete = 'DELETE  FROM '.$table;
        foreach ($where as $param => $value) {
            $sqlWhere = $param === key($where) ? " WHERE " : " OR ";
            $delete .= "{$sqlWhere}  {$value} ='{$param}'";
        }
        if ($del = $this->db->query($delete)) {
            return (bool)$this->db->affected_rows;
        }
        return false;
    }

    /**
     *  just close connection
     */
    public function __destruct()
    {
        $this->db->close();
    }
}
